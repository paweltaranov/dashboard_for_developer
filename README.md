## Физтех-школа прикладной математики и информатики МФТИ

## Итоговый курса «Инструменты анализа и визуализации данных (BI)»

Задача: Формирование дашборда для аналитики строительного предприятия "Соцжилстрой"

Инструмент: MS Excel 

**Проект(скачать xlsx-файл): [DashBoard_for_Developer](project_DashBoard_for_Developer.xlsx)**

Задание к проекту: [Project_info](https://gitlab.com/paweltaranov/dashboard_for_developer/-/blob/main/Project_info.pdf?ref_type=heads)

Источник данных: objects.rar


<!-- [Проект на https://app.powerbi.com/](https://app.powerbi.com/groups/a90ca769-5541-46ae-8380-39e54bdee95e/reports/20cebe79-59e8-4057-8fb1-e54e0f4539ba/ReportSection579597d079ae71b02845?redirectedFromSignup=1) -->

<!-- [![Typing SVG](https://readme-typing-svg.herokuapp.com?color=%2336BCF7&lines=Дашборд+Строительные+работы+девелопера+ппп)](https://git.io/typing-svg) -->

![Dashboard](dashboard_shot.jpg) 

![Microsoft](https://img.shields.io/badge/Microsoft-0078D4?style=for-the-badge&logo=microsoft&logoColor=white) 
![MicrosoftExcel](https://img.shields.io/badge/Microsoft_Excel-217346?style=for-the-badge&logo=microsoft-excel&logoColor=white) 
![GitLab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)

<!-- ![](https://komarev.com/ghpvc/?username=paweltaranov) -->
